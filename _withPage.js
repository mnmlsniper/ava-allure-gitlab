import puppeteer from 'puppeteer';

export default async function withPage(t, run) {
  const browser = await puppeteer.launch({
    headless: true,
    ignoreHTTPSErrors: true,
    defaultViewport: {
      width: 1400,
      height: 900,
    },
  });
  const page = await browser.newPage();
  try {
    await run(t, page);
  } finally {
    await page.close();
    await browser.close();
  }
}

