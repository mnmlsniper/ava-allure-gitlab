import test from 'ava';
import withPage from '../test/_withPage';

const url = 'https://google.com';

test('page title should contain "Google"', withPage, async (t, page) => {
	await page.goto(url, { waitUntil: 'networkidle2' });
	const title = await page.url();
	t.true(title.includes('google'));
});

test('this is ta thing', t => t.pass());